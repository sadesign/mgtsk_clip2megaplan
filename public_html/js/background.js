chrome.extension.onMessage.addListener(function (message, sender) {
    var re = /(task|project|contractor|deal|event)\/(\d{6,9})/;
    var str = sender.url;
    var m;
    if ((m = re.exec(str)) !== null) {
        if (message && message.type === 'showPageAction') {
            var tab = sender.tab;
            chrome.pageAction.show(tab.id);
            chrome.pageAction.setTitle({
                tabId: tab.id,
                title: 'url=' + tab.url
            });
        }
    }
});
var id = 100;
chrome.pageAction.onClicked.addListener(function () {
    chrome.tabs.captureVisibleTab(null, function (screenshotUrl) {
        var viewTabUrl = chrome.extension.getURL('screenshot.html?id=' + id++)
        var targetId = null;

        chrome.tabs.getSelected(null, function (tab) {
            //выбирается ид открытого таба, выполняется коллбек с ним
            chrome.tabs.sendMessage(tab.id, {
                screenshotUrl: screenshotUrl,
                key: localStorage['mgtskKey']
            });
            //запрос  на сообщение
        });

    });
}); 