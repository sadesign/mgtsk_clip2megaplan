chrome.extension.sendMessage({type: 'showPageAction'});
var lc;
console.log('Hi, I\'m ScreenTest');
chrome.extension.onMessage.addListener(function (req) { //обработчик запроса из background
    target = 'https://mgtsk.ru/acceptor/';
    key = req.key;
    var id = uniqid();

    var divCont = document.createElement('div');
    divCont.className = 'my-board';

    var div = document.createElement('div');
    div.id = 'board' + id;
    div.style.height = window.innerHeight + 'px';
    divCont.appendChild(div);

    var btnWr = document.createElement('div');
    btnWr.className = 'btn_wrapper';

    var sendBtn = document.createElement('a');
    sendBtn.id = 'c2m_send';
    sendBtn.className = 'tmc_btn tmc_btn_default';
    sendBtn.innerHTML = '<span class="tmc_btn-text">Отправить</span>';

    var closeBtn = document.createElement('a');
    closeBtn.id = 'c2m_close';
    closeBtn.className = 'tmc_btn tmc_btn_gray';
    closeBtn.innerHTML = '<span class="tmc_btn-text">Закрыть</span>';

    btnWr.appendChild(sendBtn);
    btnWr.appendChild(closeBtn);

    divCont.appendChild(btnWr);
    document.body.appendChild(divCont);

    var backgroundImage = new Image()
    backgroundImage.src = req.screenshotUrl;

    LC.setDefaultImageURLPrefix(chrome.extension.getURL('js/libs/literallycanvas/img'));
    lc = LC.init(
            document.getElementById('board' + id),
            {backgroundShapes: [
                    LC.createShape(
                            'Image', {x: 0, y: 0, image: backgroundImage, scale: 1}),
                ]
            }
    );
    $('body').css({'overflow': 'hidden'});
    if ($('.fotorama_fullscreen').is(':visible')) {
        $('.fotorama__fsi').click();
    }
    $(document).on('click', '#c2m_send', function () {
        $('#board' + id + ' canvas').css({'opacity': '0.5'});
        var img = lc.getImage().toDataURL('image/jpeg');
        img = img.replace('data:image/jpeg;base64,', '');
        img.replace('data:image/png;base64,', '');
        var re = /(task|project|contractor|deal|event)\/(\d{6,9})/;
        var str = window.location.pathname;
        var m;
        if ((m = re.exec(str)) !== null) {
            $.ajax({
                url: target + 'setComment',
                type: 'POST',
                data: {key: key, data: {
                        SubjectType: m[1],
                        SubjectId: m[2],
                        Model: {
//                            Text: 'Нарисоваль!',
                            Attaches: [
                                {
                                    Name: 'img_' + id + '.png',
                                    Content: img
                                }
                            ]
                        }
                    }
                },
                error: function (response) {
                    console.log(response);
                },
                success: function (response) {
                    console.log(response);
                    if (response.status.code === 'ok') {
                        $('.my-board').fadeOut(function () {
                            $('.my-board').remove();
                            $('body').css({'overflow': ''});
                            window.location.reload();
                        });
                    }
                }
            });
        }
    });
    $(document).on('click', '#c2m_close', function () {
        $('.my-board').fadeOut(function () {
            $('.my-board').remove();
            $('body').css({'overflow': ''});
        });
    });
});
function uniqid() {
    var ts = String(new Date().getTime()), i = 0, out = '';
    for (i = 0; i < ts.length; i += 2) {
        out += Number(ts.substr(i, 2)).toString(36);
    }
    return ('d' + out);
}